import React from "react";
import "./close-btn.css";
export function CloseBtn(props: { close: () => void }) {
  return (
    <button className="close-btn" onClick={props.close}>
      X
    </button>
  );
}
