import React from "react";
import { Chatroom as Room } from "../../store/types";
import "./chatroom.css";
import { CloseBtn } from "../close-btn/close-btn";
import { useDispatch } from "react-redux";
import { closeChatroom } from "../../store/actions";
export function Chatroom(prop: { room: Room }) {
  let dispatch = useDispatch();
  function close() {
    dispatch(closeChatroom(prop.room.user_id));
  }
  return (
    <div className="chatroom">
      <div className="row">
        <div className='grow'>
          Chat with {prop.room.name} (#{prop.room.user_id})
        </div>
        <CloseBtn close={close} />
      </div>
      <div className="content">
        <div className="grow">
          {prop.room.messages.map((message) => (
            <div className="row">
              <p className='grow'>{message.text}</p>
              <div>{new Date(message.time).toLocaleDateString()}</div>
            </div>
          ))}
        </div>
        <div className="inputs row">
          <input className='grow'/>
          <button>Send</button>
        </div>
      </div>
    </div>
  );
}
