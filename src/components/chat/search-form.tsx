import React from "react";
import "./search-form.css";
import { useDispatch, useSelector } from "react-redux";
import { RootState, User } from "../../store/types";
import { Action } from "redux";
import { CloseBtn } from "../close-btn/close-btn";
import { useState } from "react";
import {
  RootAction,
  saveSearchResult,
  createChatroom,
} from "../../store/actions";
export function SearchForm(props: { close: () => void }) {
  let [searchText, setSearchText] = useState("");
  let { search_result, rooms } = useSelector((state: RootState) => state);
  let dispatch = useDispatch();
  function search() {
    setTimeout(() => {
      dispatch(
        saveSearchResult([
          {
            user_id: 1,
            name: "Alice",
          },
          {
            user_id: 2,
            name: "Bob",
          },
          {
            user_id: 3,
            name: "Charlie",
          },
          {
            user_id: 20,
            name: searchText,
          },
        ])
      );
    }, 500);
  }
  function startChat(user: User) {
    setTimeout(() => {
      dispatch(
        createChatroom(user, [
          {
            sender: user.user_id,
            text: "Hi, I am " + user.name,
            time: Date.now(),
          },
          {
            sender: 10,
            text: "great let's chat",
            time: Date.now(),
          },
        ])
      );
    }, 500);
  }
  return (
    <div className="search-user">
      <div className="row">
        <div className="title grow">Start a chatroom</div>
        <CloseBtn close={props.close} />
      </div>
      <div className="row">
        <input
          value={searchText}
          onInput={(e) => setSearchText((e.target as HTMLInputElement).value)}
          placeholder="search by username"
        />
        <button onClick={search}>Search</button>
      </div>
      <div>
        <h3>Results</h3>
        {search_result.map((user) => (
          <div className="row">
            <button
              disabled={rooms.some((room) => room.user_id == user.user_id)}
              className="chat-btn"
              onClick={() => startChat(user)}
            >
              Chat
            </button>
            <div className="grow name">{user.name}</div>
          </div>
        ))}
      </div>
    </div>
  );
}
