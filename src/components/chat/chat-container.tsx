import React from "react";
import "./chat-container.css";
import { useState } from "react";
import { ChatButton } from "./chat-button";
import { SearchForm } from "./search-form";
import { useSelector } from "react-redux";
import { RootState } from "../../store/types";
import { Chatroom } from "./chatroom";
export function ChatContainer() {
  let rooms = useSelector((state: RootState) => state.rooms);
  let [isSearch, setIsSearch] = useState(false);
  return (
    <div className="chat-container">
      <div className="row">
        {rooms.map((room) => (
          <Chatroom room={room} />
        ))}
        {isSearch ? <SearchForm close={() => setIsSearch(false)} /> : undefined}
      </div>
      {!isSearch ? <ChatButton onClick={() => setIsSearch(true)} /> : undefined}
    </div>
  );
}
