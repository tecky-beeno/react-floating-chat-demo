import React from "react";
import "./chat-button.css";
export function ChatButton(props: { onClick: () => void }) {
  return (
    <button className="chat-fab" onClick={props.onClick}>
      +
    </button>
  );
}
