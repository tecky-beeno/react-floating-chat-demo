import { initState, RootState } from "./types";
import { RootAction } from "./actions";
export function reducer(
  state: RootState = initState,
  action: RootAction
): RootState {
  switch (action.type) {
    case "saveSearchResult":
      return {
        ...state,
        search_result: action.result,
      };

    case "createChatroom": {
      let rooms = state.rooms.filter(
        (room) => room.user_id !== action.user.user_id
      );
      rooms.push({
        user_id: action.user.user_id,
        name: action.user.name,
        input: "",
        messages: action.messages,
      });
      return {
        ...state,
        rooms: rooms,
      };
    }
    case "closeChatroom":
      return {
        ...state,
        rooms: state.rooms.filter((room) => room.user_id !== action.user_id),
      };
    default:
      return state;
  }
}
