import { createStore } from "redux";
import { reducer } from "./reducer";
import { RootState } from "./types";
import { RootAction } from "./actions";
export let store = createStore<RootState, RootAction, any, any>(reducer);
