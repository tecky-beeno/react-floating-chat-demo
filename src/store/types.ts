export type Message = {
  time: number;
  text: string;
  sender: number;
};
export type Chatroom = {
  user_id: number;
  name: string;
  messages: Message[];
  input:string
};
// search result
export type User = {
  user_id: number;
  name: string;
};
export type RootState = {
  token: string;
  search_result: User[];
  rooms: Chatroom[];
};
export let initState: RootState = {
  token: '',
  search_result: [],
  rooms:[],
}