import { User, Message } from "./types";

export function loginSuccess(token: string) {
  return {
    type: "loginSuccess" as "loginSuccess",
    token,
  };
}
export function searchUser(text: string) {
  return {
    type: "searchUser" as "searchUser",
    text,
  };
}
export function saveSearchResult(result: User[]) {
  return {
    type: "saveSearchResult" as "saveSearchResult",
    result,
  };
}
export function createChatroom(user: User, messages: Message[]) {
  return {
    type: "createChatroom" as "createChatroom",
    user,
    messages
  };
}
export function closeChatroom(user_id:number) {
  return {
    type: "closeChatroom" as "closeChatroom",
    user_id
  };
}
export type RootAction =
  | ReturnType<typeof loginSuccess>
  | ReturnType<typeof searchUser>
  | ReturnType<typeof saveSearchResult>
  | ReturnType<typeof createChatroom>
  | ReturnType<typeof closeChatroom>
  ;
